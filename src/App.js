import React, { Component } from 'react';
import moment from 'moment';
import './App.css';
import { TodoList } from './components/TodoList';
import { TodoHeader } from './components/TodoHeader';
import { TodoForm } from './components/TodoForm';
import { Timer } from './components/Timer';

class TodoApp extends Component {
  constructor(props) {
    super(props);
    this.state = { todoItems: [], showTimer: false };
  }

  addItem = (todoItem) => {
    const todoItems = this.state.todoItems;
    todoItems.unshift({
      index: todoItems.length + 1,
      value: todoItem.newItemValue,
      date: moment().format('ll'),
      done: false,
    });
    this.setState({ todoItems });
  };

  removeItem = (itemIndex) => {
    const todoItems = this.state.todoItems;
    todoItems.splice(itemIndex, 1);
    this.setState({ todoItems });
  };

  markTodoDone = (itemIndex) => {
    const todoItems = this.state.todoItems;
    const todo = todoItems[itemIndex];
    todoItems.splice(itemIndex, 1);
    todo.done = !todo.done;
    todo.done ? todoItems.push(todo) : todoItems.unshift(todo);
    this.setState({ todoItems });
  };

  render() {
    const { showTimer, todoItems } = this.state;
    return (
      <div id="main">
        <TodoHeader />
        <button onClick={() => this.setState({ showTimer: !showTimer })}>
          Toggle Timer
        </button>
        {showTimer && <Timer />}
        <TodoList
          items={todoItems}
          markTodoDone={this.markTodoDone}
          removeItem={this.removeItem}
        />
        <TodoForm addItem={this.addItem} />
      </div>
    );
  }
}

export default TodoApp;
