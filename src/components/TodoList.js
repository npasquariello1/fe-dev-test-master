import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TodoListItem } from './TodoListItem';

export class TodoList extends Component {
  render() {
    const { markTodoDone, removeItem } = this.props;
    const items = this.props.items.map((item, index) => {
      return (
        <TodoListItem
          index={index}
          item={item}
          key={index}
          markTodoDone={markTodoDone}
          removeItem={removeItem}
        />
      );
    });
    return <ul className="list-group"> {items} </ul>;
  }
}

TodoList.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      date: PropTypes.string,
      done: PropTypes.bool,
      index: PropTypes.number,
      value: PropTypes.string,
    })
  ),
  markTodoDone: PropTypes.func,
  removeItem: PropTypes.func,
};
