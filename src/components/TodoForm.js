import React, { Component } from 'react';
import PropTypes from 'prop-types';

export class TodoForm extends Component {
  constructor(props) {
    super(props);
    this.state = { newItemValue: '' };
    this.todoInput = React.createRef();
  }

  componentDidMount() {
    this.todoInput.current.focus();
  }

  onSubmit = (event) => {
    event.preventDefault();
    const { newItemValue } = this.state;

    if (newItemValue) {
      this.props.addItem({ newItemValue });
      this.setState({ newItemValue: '' });
    }
  };

  render() {
    return (
      <form
        ref="form"
        id="todoForm"
        onSubmit={this.onSubmit}
        className="form-inline"
      >
        <input
          type="text"
          id="itemName"
          className="form-control"
          placeholder="add a new todo..."
          ref={this.todoInput}
          onChange={(e) => this.setState({ newItemValue: e.target.value })}
          value={this.state.newItemValue}
        />
        <button type="submit" className="btn btn-default">
          Add
        </button>
      </form>
    );
  }
}

TodoForm.propTypes = {
  addItem: PropTypes.func,
};
