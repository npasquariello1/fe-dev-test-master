import React from 'react';
import renderer from 'react-test-renderer';
import { TodoHeader } from '../TodoHeader';

test('Create TodoHeader', () => {
  const component = renderer.create(<TodoHeader />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
