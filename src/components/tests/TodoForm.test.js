import React from 'react';
import renderer from 'react-test-renderer';
import { TodoForm } from '../TodoForm';

test('Create TodoForm', () => {
  const component = renderer.create(<TodoForm addItem={jest.fn()} />, {
    createNodeMock: (element) => {
      if (element.type === 'input') {
        return {
          focus: jest.fn(),
        };
      }
      return null;
    },
  });
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
