import React from 'react';
import renderer from 'react-test-renderer';
import { Timer } from '../Timer';

test('Create Timer', () => {
  const component = renderer.create(<Timer />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
