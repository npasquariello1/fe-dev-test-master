import React from 'react';
import renderer from 'react-test-renderer';
import { TodoListItem } from '../TodoListItem';

const testItem = {
  date: '1/2/22',
  done: false,
  index: 0,
  value: 'Test Todo',
};

test('Create TodoListItem', () => {
  const component = renderer.create(
    <TodoListItem
      index={0}
      item={testItem}
      markTodoDone={jest.fn()}
      removeItem={jest.fn()}
    />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
