import React from 'react';
import renderer from 'react-test-renderer';
import { TodoList } from '../TodoList';

const testItems = [
  {
    date: '1/1/22',
    done: false,
    index: 0,
    value: 'Test Todo 1',
  },
  { date: '1/2/22', done: false, index: 1, value: 'Test Todo 2' },
  {
    date: '1/3/22',
    done: true,
    index: 2,
    value: 'Test Todo 3',
  },
];

test('Create TodoList', () => {
  const component = renderer.create(
    <TodoList
      items={testItems}
      markTodoDone={jest.fn()}
      removeItem={jest.fn()}
    />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
