import React, { Component } from 'react';
import PropTypes from 'prop-types';

export class TodoListItem extends Component {
  onClickClose = () => {
    const { index, removeItem } = this.props;
    removeItem(index);
  };

  onClickDone = () => {
    const { index, markTodoDone } = this.props;
    markTodoDone(index);
  };

  render() {
    const { item } = this.props;
    const todoClass = item.done ? 'todoItem done' : 'todoItem undone';
    return (
      <li className="list-group-item">
        <div className={todoClass}>
          <span
            className="glyphicon glyphicon-ok icon"
            aria-hidden="true"
            onClick={this.onClickDone}
          />
          <span>{item.value}</span>
          <span className="date">{`Added: ${item.date}`}</span>
          <button type="button" className="close" onClick={this.onClickClose}>
            &times;
          </button>
        </div>
      </li>
    );
  }
}

TodoListItem.propTypes = {
  index: PropTypes.number,
  item: PropTypes.shape({
    date: PropTypes.string,
    done: PropTypes.bool,
    index: PropTypes.number,
    value: PropTypes.string,
  }),
  markTodoDone: PropTypes.func,
  removeItem: PropTypes.func,
};
