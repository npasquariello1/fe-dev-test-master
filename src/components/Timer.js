import React, { Component } from 'react';

export class Timer extends Component {
  constructor(props) {
    super(props);
    this.state = { count: 0 };
    this.timer = null;
  }

  componentDidMount() {
    this.timer = setInterval(this.updateTimer, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  updateTimer = () => {
    let { count } = this.state;
    this.setState({
      count: (count += 1),
    });
  };

  render = () => {
    return (
      <div>
        <h2>Seconds so Far:</h2>
        <p>{this.state.count}</p>
      </div>
    );
  };
}
